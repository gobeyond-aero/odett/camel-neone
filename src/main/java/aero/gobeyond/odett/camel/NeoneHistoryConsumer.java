package aero.gobeyond.odett.camel;

import aero.gobeyond.odett.camel.fetcher.NeoneHistoryFetcher;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.spi.ThreadPoolProfile;
import org.apache.camel.support.DefaultConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static aero.gobeyond.odett.camel.NeoneHistoryConstants.*;

public class NeoneHistoryConsumer extends DefaultConsumer {
    private static final Logger LOG = LoggerFactory.getLogger(NeoneHistoryConsumer.class);

    private final NeoneHistoryEndpoint endpoint;
    private final NeoneHistoryFetcher fetcher;
    private volatile ScheduledExecutorService executor;

    public NeoneHistoryConsumer(
            NeoneHistoryEndpoint endpoint,
            Processor processor
    ) {
        super(endpoint, processor);
        this.endpoint = endpoint;
        this.fetcher = new NeoneHistoryFetcher(endpoint);
    }

    @Override
    protected void doInit() throws Exception {
        super.doInit();

        final ThreadPoolProfile threadPoolProfile = getEndpoint()
                .getCamelContext()
                .getExecutorServiceManager()
                .getDefaultThreadPoolProfile();

        this.executor = getEndpoint()
                .getCamelContext()
                .getExecutorServiceManager()
                .newScheduledThreadPool(this, NeoneHistoryConsumer.class.getSimpleName(), threadPoolProfile);
    }

    @Override
    protected void doStart() throws Exception {
        super.doStart();

        this.executor.scheduleAtFixedRate(
                this::sendExchange,
                endpoint.getDelay(),
                endpoint.getInterval(),
                TimeUnit.MILLISECONDS
        );
    }

    private void sendExchange() {
        final Exchange exchange = createExchange(false);

        try {
            final Date now = new Date();
            exchange.getIn().setHeader(HEADER_MESSAGE_TIMESTAMP, now.getTime());
            exchange.getIn().setHeader(HEADER_FIRED_TIME, now);

            if (LOG.isTraceEnabled()) {
                LOG.trace("Polling NE:ONE server {}", endpoint.getBaseUrl());
            }

            final List<Object> changes = fetcher.call();
            // set body to poll result
            exchange.getIn().setBody(changes);
            exchange.getIn().setHeader(HEADER_FINISHED_TIME, new Date());

            getProcessor().process(exchange);
        } catch (Exception e) {
            exchange.setException(e);
        }

        // handle any thrown exception
        try {
            if (exchange.getException() != null) {
                getExceptionHandler().handleException("Error processing exchange", exchange, exchange.getException());
            }
        } finally {
            releaseExchange(exchange, false);
        }
    }

    @Override
    protected void doStop() throws Exception {
        super.doStop();
        executor.shutdownNow();
    }
}
