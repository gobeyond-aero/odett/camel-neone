package aero.gobeyond.odett.camel;

import org.apache.camel.*;
import org.apache.camel.api.management.ManagedAttribute;
import org.apache.camel.spi.Metadata;
import org.apache.camel.spi.UriEndpoint;
import org.apache.camel.spi.UriParam;
import org.apache.camel.spi.UriPath;
import org.apache.camel.support.DefaultEndpoint;


@UriEndpoint(
        firstVersion = "1.0.0",
        scheme = "neone", title = "NE:ONE", syntax = "neone:baseUrl", consumerOnly = true,
        category = {Category.TRANSFORMATION, Category.DATA},
        headersClass = NeoneHistoryConstants.class)
public class NeoneHistoryEndpoint extends DefaultEndpoint {
    @UriPath
    @Metadata(required = true, description = "Base URL of the NE:ONE server")
    private String baseUrl;
    @UriParam(defaultValue = "1000", description = "Delay before retrieving first, historical data.", javaType = "java.time.Duration")
    private long delay = 1000;

    @UriParam(defaultValue = "1000", description = "Interval between polling for new changes.",
            javaType = "java.time.Duration")
    private long interval = 1000;

    @UriParam(
            description = "OAuth2 Client Id",
            javaType = "java.lang.String"
    )
    private String clientId;

    @UriParam(
            description = "OAuth2 Client Secret",
            javaType = "java.lang.String"
    )
    private String clientSecret;

    @UriParam(
            description = "OAuth2 Scope",
            javaType = "java.lang.String"
    )
    private String scope;

    @UriParam(
            description = "OAuth2 Authorization Grant Type",
            javaType = "java.lang.String",
            defaultValue = "client_credentials"
    )
    private String authorizationGrantType = "client_credentials";

    @UriParam(
            description = "OAuth2 Access Token URL",
            javaType = "java.lang.String"
    )
    private String accessTokenUrl;

    public NeoneHistoryEndpoint(String uri, NeoneHistoryComponent neoneHistoryComponent, String baseUrl) {
        super(uri, neoneHistoryComponent);
        this.baseUrl = baseUrl;
    }
    @ManagedAttribute(description = "First Retrieval Delay")
    public void setDelay(long delay) {
        this.delay = delay;
    }

    @ManagedAttribute(description = "First Retrieval Delay")
    public long getDelay() {
        return delay;
    }

    @ManagedAttribute(description = "Poll Interval")
    public void setInterval(long interval) {
        this.interval = interval;
    }

    @ManagedAttribute(description = "Poll Interval")
    public long getInterval() {
        return interval;
    }


    @ManagedAttribute(description = "Server Base URL")
    public String getBaseUrl() {
        return baseUrl;
    }

    @ManagedAttribute(description = "Server Base URL")
    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @ManagedAttribute(description = "OAuth2 Client Id")
    public String getClientId() {
        return clientId;
    }

    @ManagedAttribute(description = "OAuth2 Client Id")
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @ManagedAttribute(description = "OAuth2 Client Secret")
    public String getClientSecret() {
        return clientSecret;
    }

    @ManagedAttribute(description = "OAuth2 Client Secret")
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @ManagedAttribute(description = "OAuth2 Scope")
    public String getScope() {
        return scope;
    }

    @ManagedAttribute(description = "OAuth2 Scope")
    public void setScope(String scope) {
        this.scope = scope;
    }

    @ManagedAttribute(description = "OAuth2 Authorization Grant Type")
    public String getAuthorizationGrantType() {
        return authorizationGrantType;
    }

    @ManagedAttribute(description = "OAuth2 Authorization Grant Type")
    public void setAuthorizationGrantType(String authorizationGrantType) {
        this.authorizationGrantType = authorizationGrantType;
    }

    @ManagedAttribute(description = "OAuth2 Access Token URL")
    public String getAccessTokenUrl() {
        return accessTokenUrl;
    }

    @ManagedAttribute(description = "OAuth2 Access Token URL")
    public void setAccessTokenUrl(String accessTokenUrl) {
        this.accessTokenUrl = accessTokenUrl;
    }

    @Override
    protected void doInit() throws Exception {
        super.doInit();
        if (baseUrl == null) {
            baseUrl = getEndpointUri();
        }
    }

    @Override
    public Producer createProducer() {
        throw new RuntimeCamelException("Cannot produce to a NeoneHistoryEndpoint: " + getEndpointUri());
    }

    @Override
    public Consumer createConsumer(Processor processor) throws Exception {
        Consumer answer = new NeoneHistoryConsumer(this, processor);
        configureConsumer(answer);
        return answer;
    }
}
