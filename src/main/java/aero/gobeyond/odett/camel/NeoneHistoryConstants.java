package aero.gobeyond.odett.camel;

import org.apache.camel.Exchange;
import org.apache.camel.spi.Metadata;

public class NeoneHistoryConstants {

    @Metadata(description = "The fired time", javaType = "Date")
    public static final String HEADER_FIRED_TIME = "NeonePollFiredTime";

    @Metadata(description = "The fired time", javaType = "Date")
    public static final String HEADER_FINISHED_TIME = "NeonePollFinishedTime";

    @Metadata(description = "The timestamp of the message", javaType = "long")
    public static final String HEADER_MESSAGE_TIMESTAMP = Exchange.MESSAGE_TIMESTAMP;
}
