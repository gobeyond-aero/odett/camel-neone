package aero.gobeyond.odett.camel.fetcher;

import aero.gobeyond.odett.camel.NeoneHistoryEndpoint;
import aero.gobeyond.odett.neone.client.okhttp.NeoneClientOkHttp;
import aero.gobeyond.odett.neone.client.okhttp.interceptor.AccessTokenAuthenticator;
import aero.gobeyond.odett.neone.client.okhttp.interceptor.AccessTokenProvider;
import aero.gobeyond.odett.neone.client.okhttp.interceptor.DefaultAccessTokenProvider;
import okhttp3.OkHttpClient;

import java.util.List;
import java.util.concurrent.Callable;

public class NeoneHistoryFetcher implements Callable<List<Object>> {
    private final Callable<List<Object>> delegate;

    public NeoneHistoryFetcher(NeoneHistoryEndpoint endpoint) {
        this.delegate = new NeoneHistoryFetcherDelegate(createNeoneClient(endpoint));
    }

    private static NeoneClientOkHttp createNeoneClient(NeoneHistoryEndpoint endpoint) {
        return new NeoneClientOkHttp(createOkHttpClient(endpoint), endpoint.getBaseUrl());
    }

    private static OkHttpClient createOkHttpClient(NeoneHistoryEndpoint endpoint) {
        return new OkHttpClient.Builder()
                .authenticator(new AccessTokenAuthenticator(createAccessTokenProvider(endpoint)))
                .build();
    }

    private static AccessTokenProvider createAccessTokenProvider(NeoneHistoryEndpoint endpoint) {
        return new DefaultAccessTokenProvider(
                new OkHttpClient.Builder().build(),
                endpoint.getClientId(),
                endpoint.getClientSecret(),
                endpoint.getAccessTokenUrl(),
                endpoint.getScope()
        );
    }

    @Override
    public List<Object> call() throws Exception {
        return delegate.call();
    }
}
