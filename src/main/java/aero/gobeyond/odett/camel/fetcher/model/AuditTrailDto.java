package aero.gobeyond.odett.camel.fetcher.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
public class AuditTrailDto {
    @SerializedName("@id")
    private String id;
    @SerializedName("@type")
    private String type;
    @SerializedName("https://onerecord.iata.org/ns/api#hasChangeRequest")
    private List<ChangeRequest> hasChangeRequest;
    @SerializedName("https://onerecord.iata.org/ns/api#hasLatestRevision")
    private TypeValue hasLatestRevision;
    @SerializedName("https://onerecord.iata.org/ns/api#isCreatedAt")
    private TypeValue isCreatedAt;
    @SerializedName("@context")
    private Context context;

    @Data
    public class ChangeRequest {
        @SerializedName("@id")
        private String id;
        @SerializedName("@type")
        private String type;
        @SerializedName("https://onerecord.iata.org/ns/api#hasChange")
        private Change hasChange;
        @SerializedName("https://onerecord.iata.org/ns/api#hasRequestStatus")
        private RequestStatus hasRequestStatus;
        @SerializedName("https://onerecord.iata.org/ns/api#isRequestedAt")
        private TypeValue isRequestedAt;
        @SerializedName("https://onerecord.iata.org/ns/api#isRequestedBy")
        private SimpleId isRequestedBy;

        @Data
        public class Change {
            @SerializedName("@id")
            private String id;
            @SerializedName("@type")
            private String type;
            @SerializedName("https://onerecord.iata.org/ns/api#hasDescription")
            private String hasDescription;
            @SerializedName("https://onerecord.iata.org/ns/api#hasLogisticsObject")
            private LogisticsObject hasLogisticsObject;
            @SerializedName("https://onerecord.iata.org/ns/api#hasOperation")
            private List<Operation> hasOperation;
            @SerializedName("https://onerecord.iata.org/ns/api#hasRevision")
            private Revision hasRevision;

            @Data
            public class Operation {
                @SerializedName("@id")
                private String id;
                @SerializedName("https://onerecord.iata.org/ns/api#o")
                private Value o;
                @SerializedName("https://onerecord.iata.org/ns/api#op")
                private SimpleId op;
                @SerializedName("https://onerecord.iata.org/ns/api#p")
                private String p;
                @SerializedName("https://onerecord.iata.org/ns/api#s")
                private String s;

                @Data
                public class Value {
                    @SerializedName("@id")
                    private String id;
                    @SerializedName("https://onerecord.iata.org/ns/api#hasDatatype")
                    private String hasDatatype;
                    @SerializedName("https://onerecord.iata.org/ns/api#hasValue")
                    private String hasValue;

                    // Getters and Setters
                }

                // Getters and Setters
            }

            @Data
            public class LogisticsObject {
                @SerializedName("@id")
                private String id;

                // Getters and Setters
            }

            @Data
            public class Revision {
                @SerializedName("@type")
                private String type;
                @SerializedName("@value")
                private int value;

                // Getters and Setters
            }

            // Getters and Setters
        }

        @Data
        public class RequestStatus {
            @SerializedName("@id")
            private String id;

            // Getters and Setters
        }

        // Getters and Setters
    }

    @Data
    public class Context {
        @SerializedName("xsd")
        private String xsd;

        // Getters and Setters
    }

    @Data
    public class SimpleId {
        @SerializedName("@id")
        private String id;
    }

    @Data
    public class TypeValue {
        @SerializedName("@type")
        private String type;
        @SerializedName("@value")
        private String value;
    }
    // Getters and Setters
}
