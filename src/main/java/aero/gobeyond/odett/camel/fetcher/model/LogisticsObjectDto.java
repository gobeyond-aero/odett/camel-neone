package aero.gobeyond.odett.camel.fetcher.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class LogisticsObjectDto {
    private String logisticsObjectURI;
    private String logisticsObjectType;
    private String latestRevision;
    private String lastModified;
}
