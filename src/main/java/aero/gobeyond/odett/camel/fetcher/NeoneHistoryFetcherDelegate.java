package aero.gobeyond.odett.camel.fetcher;

import aero.gobeyond.odett.camel.fetcher.model.LogisticsObjectDto;
import aero.gobeyond.odett.neone.client.okhttp.ApiException;
import aero.gobeyond.odett.neone.client.okhttp.NeoneClientOkHttp;
import aero.gobeyond.odett.neone.client.okhttp.api.AuditTrailControllerApi;
import aero.gobeyond.odett.neone.client.okhttp.api.ExtraLogisticsObjectsControllerApi;
import aero.gobeyond.odett.neone.client.okhttp.api.LogisticsObjectControllerApi;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Call;
import okhttp3.Response;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Callable;

@Slf4j
public class NeoneHistoryFetcherDelegate implements Callable<List<Object>> {
    private final LogisticsObjectControllerApi logisticsObjectApi;
    private final AuditTrailControllerApi auditTrailApi;
    private final ExtraLogisticsObjectsControllerApi extraLogisticsObjectApi;

    private final Cache<LogisticsObjectDto, String> logisticsObjectCache = CacheBuilder.newBuilder().build();

    public NeoneHistoryFetcherDelegate(NeoneClientOkHttp client) {
        this.logisticsObjectApi = client.logisticsObjectApi();
        this.auditTrailApi = client.auditTrailApi();
        this.extraLogisticsObjectApi = client.extraLogisticsObjectApi();
    }

    @Override
    public List<Object> call() throws Exception {
        Map<LogisticsObjectDto, String> changes = getAndCacheChangedLogisticsObjects();
        log.info("Changed LOs size: {}", changes.size());
        log.debug("Changed objects: {} ", changes);
        return new ArrayList<>(changes.values());
    }

    private Map<LogisticsObjectDto, String> getAndCacheChangedLogisticsObjects() throws IOException, ApiException {
        String extractLogisticsObjectString = getExtraLogisticsObjectData();
        Gson gson = new Gson();
        Map<String, LogisticsObjectDto> extractLogisticsObject =
                gson.fromJson(extractLogisticsObjectString, new TypeToken<Map<String, LogisticsObjectDto>>() {
                }.getType());

        Map<LogisticsObjectDto, String> changes = new HashMap<>();

        for (Map.Entry<String, LogisticsObjectDto> logisticsObjectDtoEntry : extractLogisticsObject.entrySet()) {

            String id = getIdFromURI(logisticsObjectDtoEntry.getValue().getLogisticsObjectURI());
            String detailsAsJson = getLogisticsObjectDetailsBy(id);
            LogisticsObjectDto baseLogisticsObject = logisticsObjectDtoEntry.getValue();

            if (logisticsObjectCache.getIfPresent(baseLogisticsObject) == null) {
                logisticsObjectCache.put(baseLogisticsObject, detailsAsJson);
                changes.put(baseLogisticsObject, detailsAsJson);
            }
        }
        return changes;
    }

    private String getIdFromURI(String logisticsObjectURI) {
        return logisticsObjectURI.substring(logisticsObjectURI.lastIndexOf("/") + 1);
    }

    private String getLogisticsObjectDetailsBy(String logisticsObjectId) throws ApiException, IOException {
        final Call call = logisticsObjectApi.logisticsObjectsIdGetCall(logisticsObjectId, null, null, null);
        try (Response response = call.execute()) {
            return response.body().string();
        }
    }

    private String getAuditTrailBy(String logisticsObjectId) throws ApiException, IOException {
        final Call call = auditTrailApi.logisticsObjectsIdAuditTrailGetCall(logisticsObjectId, null, null, null);
        try (Response response = call.execute()) {
            return response.body().string();
        }
    }

    private String getExtraLogisticsObjectData() throws IOException, ApiException {
        final Call call = extraLogisticsObjectApi
                //TODO: implement pagination
                .extraLogisticsObjectsGetCall(10000, Collections.emptyList(), 0, null, null);
        try (Response response = call.execute()) {
            return response.body().string();
        }
    }
}
