package aero.gobeyond.odett.camel;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Endpoint;
import org.apache.camel.spi.annotations.Component;
import org.apache.camel.support.DefaultComponent;

import java.util.Map;

@Component("neone")
@Slf4j
public class NeoneHistoryComponent extends DefaultComponent {
    @Override
    protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception {
        final NeoneHistoryEndpoint result = new NeoneHistoryEndpoint(uri, this, remaining);
        setProperties(result, parameters);
        log.info("Created endpoint: {}", uri);
        log.info("Remaining: {}", remaining);
        log.info(parameters.toString());
        return result;
    }


}
