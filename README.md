# NE:ONE Apache Camel Component to read data from NE:ONE server

This is a custom Apache Camel Component allows you to read historical data from ONE Record API and continuously get
notified about new data.

Thanks to Apache Camel you can easily add this component in your existing Camel routes and benefit from all the
features that Camel provides.

Please refer to [Apache Camel documentation](https://camel.apache.org/docs/) for more details about Camel.

## Architecture

Thanks to this Apache Camel component, you can fetch data from ONE Record API and write it to any arbitrary destination.

![architecture.png](docs%2Farchitecture.png)

## Usage

Include the following dependency in your project:

```xml
<dependency>
    <groupId>aero.gobeyond.odett</groupId>
    <artifactId>camel-neone</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

Use the component in your Camel route `neone:baseUrl?...`. Please refer to component documentation for more details.

## Future Improvement

- [ ] Replace polling with pub/sub mechanism
- [ ] Generate and Publish Camel Component Doc for the component
- [ ] Publish the component to Maven Repository
- [ ] Consider adding support for writing data, not just listening
- [ ] Add support for retrieving historical data from a given time in the past
